let x = 0, y = 0;
let x0,x1,y0,y1;
let fons = [];
let chorizo = [];
let planta = [];
let sticker = [];
let misa = [];
let rayo;
let marc;
let x2,y2,x3,y3,x4,x5,y4,y5,x6,y6;
let p1, p2, p3, p4, p5;
let i;
// let positionx = [];
// let positiony = [];


function setup() {

  let cnv = createCanvas(3976/3,5760/3);
  cnv.position(200,200);
  frameRate(0.25);



  x0 = 0;
  y0 = 0;
  x1 = 234/3;
  y1 = 400/3;


  for ( let i=0; i<15 ; i++){
    chorizo[ i ] = loadImage("./assets/tipo0" + int(i) + "-min.png");
  }

  for ( let i=0; i<8 ; i++){
    sticker[ i ] = loadImage("./assets/tecnica0" + int(i) + "-min.png");
  }

  for ( let i=0; i<18 ; i++){
    misa[ i ] = loadImage("./assets/tema0" + int(i+1) + "-min.png");
  }

  for ( let i=0; i<12 ; i++){
    planta[ i ] = loadImage("./assets/format0" + int(i) + "-min.png");
  }

  for ( let i=0; i<34 ; i++){
    fons[ i ] = loadImage("./assets/" + int(i+1) + "-min.png");
  }

  rayo = loadImage("./assets/rayo-min.png");

  marc = loadImage("./assets/marc-min.png");


}

function draw() {


  rect(0,0, width, height);

  p1=random(fons)
  p2=random(chorizo)
  p3=random(sticker)
  p4=random(planta)
  p5=random(misa)


  x2 = int(random(x1,3508/3-p2.width/4.5));
  y2 = int(random(y1,4960/3-p2.height/4.5));
  x3 = int(random(x1,3508/3-p3.width/4.5));
  y3 = int(random(y1,4960/3-p3.height/4.5));
  x4 = int(random(x1,3508/3-p4.width/4.5));
  y4 = int(random(y1,4960/3-p4.height/4.5));
  x5 = int(random(x1,3508/3-p5.width/4.5));
  y5 = int(random(y1,4960/3-p5.height/4.5));
  x6 = int(random(x1,3508/3-rayo.width/2.5));
  y6 = int(random(y1,4960/3-rayo.height/2));



  image(p1, x0, y0, 3976/3, 5760/3);
  image(p4, x4, y4, p4.width/4.5, p4.height/4.5);
  image(p2, x2, y2, p2.width/4.5, p2.height/4.5);
  image(p3, x3, y3, p3.width/4.5, p3.height/4.5);
  image(p5, x5, y5, p5.width/4.5, p5.height/4.5);
  image(rayo,x6,y6,rayo.width/2.5, rayo.height/2.5);
  image(marc, x0+20, y0+20, width-40, height-40);


}

function mousePressed(){
  console.log('mouseX: ' + mouseX);
  console.log('mouseY: ' + mouseY + '\n');

  if (
    mouseX<3648
    && mouseX>3048
    && mouseY<1751.5
    && mouseY>1295.5
  ) {
  saveCanvas('enunciat', "png");

  }

}
